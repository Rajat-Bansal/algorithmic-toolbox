#include<iostream>
#include<vector>
using namespace std;

long long maxpairwiseproduct(const vector<long long>& v, long long n){
	long long maxind1=0;
	long long maxind2=-1;
	
	for(long long i=1;i<n;i++){
		if(v[maxind1]<v[i])
		maxind1=i;
	}
	
	for(long long i=0;i<n;i++){
		if((maxind1 != i )&& (maxind2==-1 || v[maxind2]<v[i]))
		maxind2=i;
	}
	long long prod=v[maxind1]*v[maxind2];
	return prod;
}

int main(){
	long long n;
	cin>>n;
	vector<long long> v(n);
	for(long long i=0;i<n;i++)
	cin>>v[i];
	
	long long res = maxpairwiseproduct(v,n);
	cout<<res;
}
